image: tmaier/docker-compose:latest

variables:
  DOCKER_DRIVER: overlay
  HEROKU_STAGING_REGISTRY_IMAGE: registry.heroku.com/${HEROKU_STAGING}/web

services:
  - docker:dind

stages:
  - build
  - test
  - staging
  - production

build:
  stage: build
  script:
    - docker build
      --no-cache
      --build-arg DATABASE_URL="${DATABASE_URL}"
      --build-arg PYTHONUNBUFFERED="${PYTHONUNBUFFERED}"
      --build-arg APP_SETTINGS="${APP_SETTINGS}"
      --build-arg APP_SECRET_KEY="${APP_SECRET_KEY}"
      --build-arg MAIL_PASSWORD="${MAIL_PASSWORD}"
      --build-arg MAIL_PORT="${MAIL_PORT}"
      --build-arg MAIL_SERVER="${MAIL_SERVER}"
      --build-arg MAIL_USERNAME="${MAIL_USERNAME}"
      --build-arg SECURITY_CONFIRM_SALT="${SECURITY_CONFIRM_SALT}"
      --build-arg SECURITY_EMAIL_SENDER="${SECURITY_EMAIL_SENDER}"
      --build-arg SECURITY_PASSWORD_SALT="${SECURITY_PASSWORD_SALT}"
      --build-arg SECURITY_RESET_SALT="${SECURITY_RESET_SALT}" "."
  only:
    - merge_requests
    - master

coverage:
  stage: test
  image: python:3.7
  script:
    - pip install -r requirements/production.txt
    - flask db upgrade
    - coverage run --source=app,manage,config -m unittest discover && coverage report -m
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  only:
    - merge_requests
    - master

code quality:
  stage: test
  image: python:3.7
  script:
    - pip install -r requirements/production.txt
    - pip install pylint-exit pylint-flask pylint-sqlalchemy anybadge
    - mkdir ./pylint
    - pylint --load-plugins=pylint_flask,pylint_sqlalchemy --exit-zero --output-format=text app test config.py manage.py | tee ./pylint/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint.log)
    - anybadge --label=Pylint --file=./pylint/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 8=yellow 10=green
    - echo "Pylint score is $PYLINT_SCORE"
  artifacts:
    paths:
      - ./pylint/
  only:
    - merge_requests
    - master

staging:
  stage: staging
  script:
    - docker build
      --no-cache
      --tag "$HEROKU_STAGING_REGISTRY_IMAGE"
      --build-arg DATABASE_URL="${STAGING_DATABASE_URL}"
      --build-arg PYTHONUNBUFFERED="${PYTHONUNBUFFERED}"
      --build-arg APP_SETTINGS="${APP_SETTINGS}"
      --build-arg APP_SECRET_KEY="${APP_SECRET_KEY}"
      --build-arg MAIL_PASSWORD="${MAIL_PASSWORD}"
      --build-arg MAIL_PORT="${MAIL_PORT}"
      --build-arg MAIL_SERVER="${MAIL_SERVER}"
      --build-arg MAIL_USERNAME="${MAIL_USERNAME}"
      --build-arg SECURITY_CONFIRM_SALT="${SECURITY_CONFIRM_SALT}"
      --build-arg SECURITY_EMAIL_SENDER="${SECURITY_EMAIL_SENDER}"
      --build-arg SECURITY_PASSWORD_SALT="${SECURITY_PASSWORD_SALT}"
      --build-arg SECURITY_RESET_SALT="${SECURITY_RESET_SALT}" "."
    - docker login -u ${HEROKU_USERNAME} -p ${HEROKU_KEY} registry.heroku.com
    - docker push $HEROKU_STAGING_REGISTRY_IMAGE
    - docker run -p "80:$PORT" --rm -e HEROKU_API_KEY=${HEROKU_KEY} wingrunr21/alpine-heroku-cli container:release web --app ${HEROKU_STAGING}
  when:
    manual
  only:
    - merge_requests
    - master

production:
  stage: production
  before_script:
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - export DOCKER_HOST="ssh://${PROD_SERVER_USER}@${PROD_SERVER_HOST}"
  script:
    - ssh -o "StrictHostKeyChecking no" ${PROD_SERVER_USER}@${PROD_SERVER_HOST} 'sudo yum -y update && sudo yum -y upgrade'
    - docker-compose down
    - docker-compose rm -fs && docker system prune -fa
    - docker-compose up -d --remove-orphans
  when:
    manual
  only:
    - merge_requests
    - master
