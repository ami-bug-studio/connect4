#!python3

import os

from werkzeug.utils import import_string

from app import create_app, session
from app.role.model import Role


config = import_string(os.environ['APP_SETTINGS'])()
# init class object so properties can work

app = create_app(config)


@app.before_first_request
def add_initial_player_role():
    """Add initial player role."""

    if not session.query(Role).filter(Role.name == 'player').first():
        session.add(Role(name='player', description='Normal player entity'))
        session.commit()


if __name__ == '__main__':
    app.run()
