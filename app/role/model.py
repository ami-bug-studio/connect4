from sqlalchemy import Integer, String, Column

from app.database import Base


class Role(Base.Model):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    name = Column(String(15), unique=True, nullable=False)
    description = Column(String(255), unique=False, nullable=True)

    def __str__(self):
        return f"Role {self.id}: {self.name}"
