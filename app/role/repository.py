from app.role.model import Role as RoleModel
from app.basic_repository import BasicRepository


class RoleRepository(BasicRepository):
    model = RoleModel

    @classmethod
    def get_by_name(cls, name):
        return super().get_by_field("name", name)

    @classmethod
    def get_by_id(cls, _id):
        return super().get_by_field("id", _id)
