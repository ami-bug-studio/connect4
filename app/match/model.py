"""Define orm-mapper for Match entity."""

from sqlalchemy import Integer, Column, DateTime, CheckConstraint
from sqlalchemy.orm import relationship

from app.database import Base


class Match(Base.Model):
    __tablename__ = "matches"

    id = Column(Integer, primary_key=True)
    completionDate = Column(DateTime, nullable=True)
    difficulty = Column(Integer, nullable=False)
    fieldHeight = Column(Integer, nullable=False)
    fieldWidth = Column(Integer, nullable=False)
    startDate = Column(DateTime, nullable=True)

    players = relationship("Player", lazy="select",
                           cascade="save-update, delete, delete-orphan")

    def __str__(self):
        return f"Match(id={self.id}, startDate={self.startDate})"
