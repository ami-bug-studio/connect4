"""Match managing service."""

import datetime
import typing

from app.exceptions import (
    InstanceNotFoundException,
    MatchCfgException,
    MatchServiceAccessException,
    MatchServiceException,
)
from app.match.model import Match
from app.match.repository import MatchRepository
from app.match.schema import MatchSchema
from app.player.repository import PlayerRepository

MATCH_SCHEMA = MatchSchema(load_only="players", unknown="EXCLUDE")


class MatchService:
    """Collection of Match specific service methods."""

    class Defaults:
        """Config defaults constants store."""

        field_sizes = (7, 6), (5, 7), (9, 7)  # (width, height)
        difficulty = range(0, 3)

    @classmethod
    def _create(cls, match: Match) -> Match:
        """Initialize and return Match from preconfigured Match instance with 2
        appended players.
         """
        # if len(match.players) != 2:
        #     raise MatchServiceException("Match must have 2 players.")
        #
        # if match.players[0] == match.players[1]:
        #     raise TypeError("Player A is the same as Player B.")

        if (match.fieldWidth, match.fieldHeight) not in \
                cls.Defaults.field_sizes:
            raise MatchCfgException("Incorrect field size.")

        match = MatchRepository.create(match)

        return match

    @classmethod
    def get_current(cls, user_id: int) -> typing.Optional[Match]:
        """Return uncompleted match (if any) for user.
        Here uncompleted is one that hasn't completion date set.
        """
        for match in MatchRepository.get_by_user(user_id):
            if not match.completionDate:
                return match

    @classmethod
    def is_user_playing(cls, user_id: int) -> bool:
        """Check that user has an active match. Here active is one that
         has start but not completion date set.
         """
        users_matches = [
            MatchRepository.get_by_player(player.id)
            for player in PlayerRepository.get_by_user(user_id)
        ]

        # all() of empty list (generator) is True...
        return users_matches and all(
            match.startDate or match.completionDate
            for match in users_matches
        )

    @classmethod
    def start(cls, match_id: int) -> None:
        """Set match start date to current utc time.

        `Raise MatchServiceException` if match hasn't been set 2 players or
         if it already has tart date.
        """
        match = cls._get_model_or_rise(match_id)

        if len(match.players) != 2:
            raise MatchServiceException(
                "Match should have 2 players to start.")

        if match.startDate:
            raise MatchServiceException(
                "Match start date is already assigned.")

        MatchRepository.update(match_id, startDate=datetime.datetime.utcnow())

    @classmethod
    def delete_unbegun(cls, match_id: int, user_id: int = None) -> None:
        """Delete the Match if it hasn't start date, i.e. unbegun. Check that
        the Match linked to User if `user_id` is provided."""
        match = cls._get_model_or_rise(match_id)

        if user_id and not any(player.user_id == user_id for player in \
                match.players):
            raise MatchServiceAccessException(
                "The user can't remove foreign match."
            )

        if match.startDate:
            raise MatchServiceException(
                "Can't delete as unbegun, match has started.")

        MatchRepository.delete(match)

    @classmethod
    def get(cls, match_id: int, user_id: int = None) -> dict:
        """Return Match by match_id.

        Raise `MatchServiceAccessException` if `user_id` is provided
        and `user_id` isn't linked to the Match."""
        match = cls._get_model_or_rise(match_id)

        if user_id and not any(
                player.user_id == user_id for player in match.players):
            raise MatchServiceAccessException(
                "The user isn't allowed to view the match."
            )

        return MATCH_SCHEMA.dump(match)

    @classmethod
    def _get_model_or_rise(cls, match_id: int) -> Match:
        match = MatchRepository.get(match_id)

        if not match:
            raise InstanceNotFoundException("No match with such match_id.")

        return match

    @classmethod
    def create_singleplayer(cls, match: Match, user_id: int) -> dict:
        """Create a new match from . Check that user hasn't any active ongoing
         match. If user has match that hasn't start date (i.e. not ongoing) -
         remove that match and _create new one.
        Test that user don't have any ongoing match. Assign user's newly created
         player and AI's player to the match.

        :param user_id: user's id
        :param match: Match instance with filled required fields (those from
        `MatchService._create()`).
        Used only as configuration container
        :return: Match object dumped to dict using marshmallow_scheme
        """
        if match.difficulty not in cls.Defaults.difficulty:
            raise MatchCfgException("Incorrect difficulty")

        match = cls._create(match)

        if cls.is_user_playing(user_id):
            raise MatchServiceException(
                "Cannot _create a new game. User has incomplete match."
            )

        current = cls.get_current(user_id)
        if current:
            cls.delete_unbegun(current.id, user_id)

        player = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user_id=user_id, match=match
        )

        # TODO: replace with a real AI Player
        ai = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user_id=user_id, match=match
        )

        match.players.extend([player, ai])

        return MATCH_SCHEMA.dump(cls._create(match))
