"""Provide flask-Marshmallow schema for Match model."""

from marshmallow.fields import Pluck

from app.database import marshmallow
from app.match.model import Match
from app.player.schema import PlayerSchema


class MatchSchema(marshmallow.ModelSchema):
    """Match schema with inclusion of Players as simple list of identifiers."""

    class Meta:
        model = Match
        dump_only = ("id", "players", "startDate", "completionDate")
        include_fk = False

    players = Pluck(PlayerSchema(only=["id"]), "id", many=True)
