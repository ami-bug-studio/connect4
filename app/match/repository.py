"""Handlers for basic Match ORM-requests."""

import typing

from app.basic_repository import BasicRepository
from app.exceptions import RepositorySQLAlchemyError
from app.player.model import Player
from app.user.model import User
from app.match.model import Match
from app import session
from app.player.repository import PlayerRepository


class MatchRepository(BasicRepository):
    """Collection of simple Match ORM handlers."""

    model = Match

    @classmethod
    def create(cls, match: Match) -> model:

        session.add(match)
        session.commit()

        return match

    @classmethod
    def get_by_player(cls, player_id) -> typing.Optional[Match]:
        """Return linked with provided by player_id Player Match."""
        return (
            session.query(Match)
            .join(Player)
            .filter(Player.id == player_id)
            .one_or_none()
        )

    @classmethod
    def get_by_user(cls, user_id) -> typing.List[Match]:
        """Return linked with provided by user_id User Match."""
        return (
            session.query(Match)
            .join(Player)
            .join(User)
            .filter(User.id == user_id)
            .all()
        )
