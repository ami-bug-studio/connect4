from flask import Blueprint, request
from flask_restful import Resource, Api

from app.user.service import UserService
from app.exceptions import (
    AuthenticationException,
    RegistrationException,
    RegistrationFormValidationError,
    UnconfirmedUserException,
    UserIsntExistError,
)
from app.views import json_required

auth_rest_bp = Blueprint("Auth_rest", __name__)

auth_rest = Api(auth_rest_bp)


@auth_rest.resource("/login")
class Login(Resource):
    @json_required
    def post(self):
        login = request.json.get("login", None)
        password = request.json.get("password", None)
        try:
            access_token, user_auth_dto = UserService.authenticate_user(login,
                                                                        password)
        except UnconfirmedUserException:
            return {"msg": "Provided account isn't activated."}, 401

        except AuthenticationException:
            return {"msg": "No active user for such login-password pair."}, 401

        return {"access_token": access_token,
                "user": user_auth_dto.to_dict()}, 200


@auth_rest.resource("/register")
class UserRegistration(Resource):
    @json_required
    def post(self):
        email = request.json.get("email", None)
        login = request.json.get("login", None)
        password = request.json.get("password", None)

        try:
            UserService.register_user(email, login, password)

        except RegistrationFormValidationError as exc:
            return {"errors": exc.errors}, 400

        except RegistrationException:
            return {"msg": "Error during registration"}, 500

        return (
            {
                "msg": " Registration is successful. "
                       "Check your email for activation link."
            },
            202,
        )


@auth_rest.resource("/password_restore")
class PasswordRestore(Resource):
    @json_required
    def post(self):
        email = request.json.get("email", None)
        login = request.json.get("login", None)

        if bool(email) == bool(login):
            return (
                {"msg": "Insufficient arguments. Provide email or login only"},
                400,
            )

        try:
            UserService.password_restore(email, login)
        except UserIsntExistError:
            return {"msg": "No user with such credentials"}, 400

        return {"msg": "Recovery instructions have been sent to user's email"},\
            200
