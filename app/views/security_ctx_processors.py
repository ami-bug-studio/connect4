# list of all context processors:
# https://pythonhosted.org/Flask-Security/customizing.html#forms


def forgot_password_ctx_proc():
    return dict(title="Forgot Password")


def reset_password_ctx_proc():
    return dict(title="Reset Password")


def send_confirmation_ctx_proc():
    return dict(title="Send confirmation")
