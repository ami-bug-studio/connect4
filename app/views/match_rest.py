"""Provide REST-like API for Match entity handling."""

from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from flask_restful import Api, Resource
from flask_security import current_user, roles_required
from marshmallow import ValidationError

from app.exceptions import MatchServiceAccessException, MatchServiceException
from app.match.service import MATCH_SCHEMA, MatchService
from app.views import json_required

MATCH_REST_BP = Blueprint("Match_rest", __name__)
MATCH_REST = Api(MATCH_REST_BP)


@MATCH_REST.resource("/<int:match_id>", "")
class Match(Resource):
    """Handle specific Match entity."""

    @json_required
    @jwt_required
    @roles_required("player")
    def get(self, match_id=None):
        """Return match instance by match_id.

       `HTTP-200 and Match instance` if handled successfully.

       `HTTP-401 and error msg` in case of requesting foreign to user Match or
        for any other error.

       `HTTP-404 and error msg` if there is no Match with such id.
       """
        if not match_id:
            raise NotImplementedError("Returning multiple instances.")

        try:
            match = MatchService.get(match_id, current_user.id)

        except MatchServiceAccessException as exc:
            return {"msg": str(exc)}, 401

        except MatchServiceException as exc:
            return {"msg": str(exc)}, 400

        return {"match": match}, 200

    @json_required
    @jwt_required
    @roles_required("player")
    def post(self):
        """Begin SinglePlayer or Multiplayer match.

        Use user provided match object with basic match settings _create match and return
        updated match instance with match_id and players list.
        """
        type = request.args.get("type", default="singleplayer", type=str)
        if type not in ("singleplayer", "multiplayer"):
            return {"msg": "Provide correct match type "
                           "(singleplayer or multiplayer).",
                    "provided_type": type}, 400

        match_dict = request.json.get("match")
        if not match_dict:
            return {"msg": "Provide match field."}, 400

        try:
            match = MATCH_SCHEMA.load(match_dict)
        except ValidationError as exc:
            return {"msg": "Match deserialization failed.",
                    "failed fields": str(exc)}, 400

        try:
            if type == "singleplayer":
                match_dump = MatchService.create_singleplayer(
                    match, current_user.id
                )

                return {"match": match_dump}, 200

            else:
                raise NotImplementedError("Multiplayer.")

        except MatchServiceException as exc:
            return {"msg": str(exc)}, 400


@MATCH_REST.resource("/current")
class CurrentMatch(Resource):
    @json_required
    @jwt_required
    @roles_required("player")
    def get(self):
        """Return match instance of currently active match of request initializer user.

        HTTP-200 and match active match exists.

        HTTP-204 otherwise.
        """
        match = MatchService.get_current(current_user.id)

        if match:
            return {"match": MATCH_SCHEMA.dump(match)}, 200

        return None, 204


@MATCH_REST.resource("/stop_match")
class StopMatch(Resource):
    """Provide resource for stopping active match."""

    @json_required
    @jwt_required
    @roles_required("player")
    def post(self):
        """Serve request for match stopping.

        Delete if match if it hasn't been started. No any other use cases.

        `HTTP-404 and msg` if no any active match for the user.

        `HTTP-401 and msg` if user requested foreign match.

        `HTTP-400 and msg` if match has been started or for any other error.
        """
        match = MatchService.get_current(current_user.id)

        if not match:
            return {"msg": "No any active match to stop."}, 404

        try:
            MatchService.delete_unbegun(match.id, current_user.id)

        except MatchServiceAccessException as exc:
            return {"msg": str(exc)}, 401

        except MatchServiceException as exc:
            return {"msg": str(exc)}, 400

        return {"msg": "The match has been stopped."}, 200
