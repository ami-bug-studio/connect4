from flask import Blueprint, render_template

greetings_blueprint = Blueprint("Greetings", __name__)


@greetings_blueprint.route("/email_confirmation_finished")
def after_email_confirmation_view():
    return (
        render_template(
            "user-api/email_confirmation_finished.html",
            title="Congratulations for account confirmation!",
        ),
        200,
    )


@greetings_blueprint.route("/password_reset_finished")
def after_password_reset_view():
    return (
        render_template(
            "user-api/password_reset_finished.html",
            title="Your password has been changed",
        ),
        200,
    )
