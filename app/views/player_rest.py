"""Provide REST-like API for Player entity handling."""

from flask import Blueprint
from flask_jwt_extended import jwt_required
from flask_restful import Api, Resource
from flask_security import current_user, roles_required

from app.exceptions import (
    InstanceNotFoundException,
    PlayerServiceAccessException
)
from app.player.service import PlayerService
from app.views import json_required

PLAYER_REST_BP = Blueprint("Player_rest", __name__)

PLAYER_REST = Api(PLAYER_REST_BP)


@PLAYER_REST.resource("/<int:player_id>")
class Player(Resource):
    """Handle specific Player entity."""

    @json_required
    @jwt_required
    @roles_required("player")
    def get(self, player_id):
        """Return player instance by player_id.

        `HTTP-200 and Player instance` if handled successfully.

        `HTTP-401 and error msg` in case of requesting foreign to user player or for any
        other error.

        `HTTP-404 and error msg` if there is no user with such id.
        """
        try:
            return (
                {
                    "player": PlayerService.get_player(
                        player_id, current_user.id
                    )
                },
                200,
            )

        except PlayerServiceAccessException as exc:
            return {"msg": str(exc)}, 401

        except InstanceNotFoundException as exc:
            return {"msg": str(exc)}, 404
