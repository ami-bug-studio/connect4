from functools import wraps
from threading import Thread

from flask import request, abort
from flask_security.utils import login_user

from app.database import jwt
from app.database import mail
from app.user.service import UserService


def json_required(func):
    @wraps(func)
    def inner(*vargs, **kwargs):
        if not request.is_json:
            return abort(406)

        return func(*vargs, **kwargs)

    return inner


@jwt.user_loader_callback_loader
def authorize_user_by_id(user_id):
    if not user_id:
        return abort(400)

    user = UserService.get_by_id(user_id)
    if not user:
        return abort(400)

    login_user(user)

    return user_id


def _send_email_in_ctx(message):
    from manage import app

    # available to import only during the runniest run time

    with app.app_context():
        mail.send(message)


# Decorated manually like `@security.send_mail_task` in app/__init__.py to
# prevent
# recursion.
def send_async_email(message):
    Thread(target=_send_email_in_ctx, args=(message,)).start()
