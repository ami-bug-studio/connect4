import logging
from abc import ABC, abstractproperty

from flask_sqlalchemy import Pagination
from sqlalchemy import asc, desc
from sqlalchemy.exc import SQLAlchemyError

from app.database import session
from app.exceptions import RepositorySQLAlchemyError


class BasicRepository(ABC):
    model = property()

    @classmethod
    def create(cls, _commit=True, **fields) -> model:
        obj = cls.model(**fields)

        if _commit:
            session.add(obj)
            session.commit()
            session.refresh(obj)

        return obj

    @classmethod
    def update(cls, id_, **fields):
        obj = cls.get_or_raise(id_)

        for field, value in fields.items():
            if not hasattr(obj, field):
                raise KeyError(f"No such field as {field} in {cls.model}")

            setattr(obj, field, value)

        session.add(obj)
        session.commit()
        session.refresh(obj)

    @classmethod
    def extent_list_field(cls, id_, field, values):
        obj = cls.get_or_raise(id_)
        model_attr = getattr(obj, field)
        if not isinstance(model_attr, list):
            raise RepositorySQLAlchemyError("Provided field is not a list "
                                            "instance.")

        if isinstance(values, list):
            model_attr.extend(values)
        else:
            model_attr.append(values)

        session.add(obj)
        session.commit()

    @classmethod
    def get_or_raise(cls, id_):
        obj = session.query(cls.model).get(id_)
        if not obj:
            raise RepositorySQLAlchemyError("Object is not found.")

        return obj

    @classmethod
    def save(cls, obj) -> model:
        if obj is not None:
            try:
                session.add(obj)
                session.commit()
                session.refresh(obj)
                return obj
            except SQLAlchemyError as ex:
                logging.error("An error occurred while save method was called: %s", ex)
                session.rollback()

    @classmethod
    def save_by_attrs(cls, **attrs) -> model:
        return cls.save(cls.model(**attrs))

    @classmethod
    def save_all(cls, objects: []) -> list:
        if objects is not None and len(objects) > 0:
            try:
                session.add_all(objects)
                session.commit()
                for _obj in objects:
                    session.refresh(_obj)
                return objects
            except SQLAlchemyError as ex:
                logging.error(
                    "An error occurred while save_all_obj method was called: %s", ex
                )
        return []

    @classmethod
    def delete(cls, obj) -> bool: # bool - crashed or not?..
        if obj is not None:
            try:
                session.delete(obj)
                session.commit()
                return True
            except SQLAlchemyError as exc:
                logging.error(
                    "An error occurred while delete method was called: %s", exc
                )
                session.rollback()
                raise exc
        return False

    @classmethod
    def delete_all(cls, objects: []) -> bool:
        if objects is not None and len(objects) > 0:
            try:
                for _obj in objects:
                    session.delete(_obj)
                session.commit()
                return True
            except SQLAlchemyError as ex:
                logging.error(
                    "An error occurred while delete_all method was called: %s", ex
                )
                # TODO: check, does rollback really need here after switching to flask-sqlalchemy
                session.rollback()

        return False

    @classmethod
    def get_by_field(cls, field: str, value) -> model:
        if hasattr(cls.model, field):
            try:
                return (
                    session.query(cls.model)
                    .filter(getattr(cls.model, field) == value)
                    .first()
                )
            except SQLAlchemyError as ex:
                #  TODO: check, should and, if yes, when should we propagate exceptions,
                #   so flask-sqlalchemy successfully ends session
                logging.error(
                    "An error occurred while get_by_field method was called: %s", ex
                )

    @classmethod
    def get_all_by_field(cls, field: str, value) -> list:
        if hasattr(cls.model, field):
            try:
                return (
                    session.query(cls.model)
                    .filter(getattr(cls.model, field) == value)
                    .all()
                )
            except SQLAlchemyError as ex:
                logging.error(
                    "An error occurred while get_all_by_field method was called: %s",
                    ex,
                )
        return []

    @classmethod
    def get_all_by_field_pageable(
        cls, field: str, value, sort_fields: {}, page: int = 1, per_page: int = 10
    ) -> Pagination:
        if hasattr(cls.model, field):
            try:
                field_attr = getattr(cls.model, field)
                attr = cls.attr_sort_list_create(sort_fields)
                if len(attr) > 0:
                    return (
                        cls.model.query.filter(field_attr == value)
                        .order_by(*attr)
                        .paginate(page, per_page, error_out=False, max_per_page=100)
                    )
                else:
                    return cls.model.query.filter(field_attr == value).paginate(
                        page, per_page, error_out=False, max_per_page=100
                    )
            except SQLAlchemyError as ex:
                logging.error(
                    "An error occurred while"
                    " get_all_by_field_pageable method was called: %s",
                    ex,
                )

    @classmethod
    def get(cls, _id: int) -> model:
        if _id is not None:
            try:
                return session.query(cls.model).get(_id)
            except SQLAlchemyError as exc:
                logging.error(
                    "An error occurred while get_by_id method was called: %s", exc
                )
                raise exc

    @classmethod
    def find_all_by_field(cls, field: str, value) -> list:
        if hasattr(cls.model, field):
            try:
                return (
                    session.query(cls.model)
                    .filter(getattr(cls.model, field).ilike(f"{value}%"))
                    .all()
                )
            except SQLAlchemyError as exc:
                logging.error(
                    "An error occurred while"
                    " find_obj_by_field method was called: %s",
                    exc,
                )
                raise exc

    @classmethod
    def find_all_by_field_pageable(
        cls, field: str, value, sort_fields: {}, page: int = 1, per_page: int = 10
    ) -> Pagination:
        if hasattr(cls.model, field):
            try:
                field_attr = getattr(cls.model, field)
                attr = cls.attr_sort_list_create(sort_fields)
                if len(attr) > 0:
                    return (
                        cls.model.query.filter(field_attr.ilike(f"{value}%"))
                        .order_by(*attr)
                        .paginate(page, per_page, error_out=False, max_per_page=100)
                    )
                else:
                    return cls.model.query.filter(
                        field_attr.ilike(f"{value}%")
                    ).paginate(page, per_page, error_out=False, max_per_page=100)
            except SQLAlchemyError as ex:
                logging.error(
                    "An error occurred while"
                    " find_all_by_field_pageable method was called: %s",
                    ex,
                )

    @classmethod
    def attr_sort_list_create(cls, sort_fields: {}):
        sort_list = []
        if sort_fields is not None:
            i = 0
            keys = list(sort_fields.keys())
            while i < len(sort_fields):
                if hasattr(cls.model, keys[i]):
                    if list(sort_fields.values())[i] == "desc":
                        sort_list.append(desc(getattr(cls.model, keys[i])))
                    else:
                        sort_list.append(asc(getattr(cls.model, keys[i])))
                i += 1
        return sort_list
