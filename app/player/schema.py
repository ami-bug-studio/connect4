"""Provide flask-Marshmallow schema for Player model."""

from app import marshmallow
from app.player.model import Player


class PlayerSchema(marshmallow.ModelSchema):
    """Player Schema"""

    class Meta:
        model = Player
        dump_only = ("id",)
        include_fk = False
