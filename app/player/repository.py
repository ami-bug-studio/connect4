"""Handlers for basic Player ORM-requests."""
import typing

from app import session
from app.basic_repository import BasicRepository
from app.match.model import Match
from app.player.model import Player


class PlayerRepository(BasicRepository):
    """Collection of basic wrappers for Player ORM-handlers."""

    model = Player

    @classmethod
    def get_by_match(cls, match_id) -> typing.List[typing.Optional[Player]]:
        """Return Players linked with the Match."""
        return session.query(Player).join(Match)\
            .filter(Match.id == match_id).all()

    @classmethod
    def get_by_user(cls, user_id) -> typing.List[typing.Optional[Player]]:
        """Return Players linked with the User."""
        return session.query(Player).filter(Player.user_id == user_id).all()
