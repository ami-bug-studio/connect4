"""Player managing service."""

from app.exceptions import (
    InstanceNotFoundException,
    PlayerServiceAccessException
)
from app.player.model import Player
from app.player.repository import PlayerRepository
from app.player.schema import PlayerSchema


class PlayerService:
    """Collection of Player specific service methods."""

    @classmethod
    def get_player(cls, player_id: int, user_id: int = None) -> Player:
        """Return Player instance marshmallow dump.
        If user_id is provided then check that User linked to the same Match
         as requested Player fails and `raise PlayerServiceAccessException`
         if this constraint hasn't satisfied.


        Raise `InstanceNotFoundException` if there is no such Player.
        """
        player = PlayerRepository.get(player_id)
        if not player:
            raise InstanceNotFoundException("Player isn't found.")

        if user_id and player.user_id != user_id:
            # check that user's Player linked to the same game as requested
            # Player
            if (not player.match or not any(
                    player.user_id == user_id for player in
                    player.match.players)):
                raise PlayerServiceAccessException(
                    "The player isn't attached to any user's match."
                )

        return PlayerSchema().dump(player)
