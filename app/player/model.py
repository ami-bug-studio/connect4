"""Define orm-mapper for Player (PlayerInstance) entity."""

from sqlalchemy import Integer, Column, ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref

from app.database import Base


class Player(Base.Model):
    __tablename__ = "players"

    id = Column(Integer, primary_key=True)
    is_ai = Column(Boolean, default=False)
    is_winner = Column(Boolean, default=False)
    score = Column(Integer, nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    match_id = Column(Integer, ForeignKey("matches.id"), nullable=False)

    user = relationship(
        "User",
        backref=backref("players", lazy="select"),
        uselist=False,
        single_parent=True
    )

    match = relationship("Match", lazy="select", uselist=False,
                         single_parent=True)

    def __str__(self):
        return f"Player(id={self.id}, user_id={self.user_id})"
