from flask import Flask
from flask_security import SQLAlchemyUserDatastore
from sqlalchemy import create_engine

from app.database import Base, jwt, marshmallow, migrate, security, mail, session, babel
from app.role.model import Role as RoleModel
from app.role.repository import RoleRepository
from app.user.model import User as UserModel
from app.player.model import Player
from app.match.model import Match
from app.views import send_async_email
from app.views.auth_rest import auth_rest_bp
from app.views.html_views import greetings_blueprint
from app.views.match_rest import MATCH_REST_BP
from app.views.player_rest import PLAYER_REST_BP
from app.views.security_ctx_processors import (
    forgot_password_ctx_proc,
    reset_password_ctx_proc,
    send_confirmation_ctx_proc,
)
from config import Config

DEBUG = None


def create_app(config: Config) -> Flask:
    """Create and configure project's Flask app object using Config instance."""

    app = Flask(__name__)  # root_path='app/'
    # https://flask.palletsprojects.com/en/1.0.x/api/#application-object for
    # details about static and template paths
    app.config.from_object(config)

    babel.init_app(app)

    global DEBUG
    DEBUG = app.debug

    Base.init_app(app)
    app.session = database.session

    migrate.init_app(app, Base)

    marshmallow.init_app(app)

    mail.init_app(app)

    jwt.init_app(app)

    # TODO: Warning! this looks bad.
    app.config['PROPAGATE_EXCEPTIONS'] = True
    jwt._set_error_handler_callbacks(app)

    user_datastore = SQLAlchemyUserDatastore(app, UserModel, RoleModel)

    # Bug fix for https://github.com/mattupstate/flask-security/issues/783
    # security._state = app.extensions['security']
    security.init_app(app, user_datastore)
    setattr(security, "_state", app.extensions["security"])

    security.send_mail_task(send_async_email)
    security.forgot_password_context_processor(forgot_password_ctx_proc)
    security.reset_password_context_processor(reset_password_ctx_proc)
    security.send_confirmation_context_processor(send_confirmation_ctx_proc)

    create_engine(app.config["SQLALCHEMY_DATABASE_URI"])

    app.register_blueprint(auth_rest_bp, url_prefix="/api/auth")
    app.register_blueprint(MATCH_REST_BP, url_prefix="/api/matches")
    app.register_blueprint(PLAYER_REST_BP, url_prefix="/api/players")
    app.register_blueprint(greetings_blueprint, url_prefix="")

    return app
