from flask_sqlalchemy import Pagination

from app.basic_repository import BasicRepository
from app.user.model import User as UserModel


class UserRepository(BasicRepository):
    model = UserModel

    @classmethod
    def get_by_id(cls, _id):
        return super().get_by_field("id", _id)

    @classmethod
    def get_by_login(cls, login):
        return super().get_by_field("login", login)

    @classmethod
    def get_by_email(cls, email):
        return super().get_by_field("email", email)

    @classmethod
    def get_all_active(cls) -> list:
        return super().get_all_by_field("active", True)

    @classmethod
    def get_all_active_pageable(
        cls, sort_fields: {}, page: int, per_page: int
    ) -> Pagination:
        return super().get_all_by_field_pageable(
            "active", True, sort_fields, page, per_page
        )

    @classmethod
    def find_all_by_login(cls, login: str) -> list:
        return super().find_all_by_field("login", login)

    @classmethod
    def find_all_by_email(cls, email: str) -> list:
        return super().find_all_by_field("email", email)

    @classmethod
    def find_all_by_login_pageable(
        cls, login: str, sort_fields: {}, page: int, per_page: int
    ) -> Pagination:
        return super().find_all_by_field_pageable(
            "login", login, sort_fields, page, per_page
        )

    @classmethod
    def find_all_by_email_pageable(
        cls, email: str, sort_fields: {}, page: int, per_page: int
    ) -> Pagination:
        return super().find_all_by_field_pageable(
            "email", email, sort_fields, page, per_page
        )

    @classmethod
    def exists(cls, _id: int) -> bool:
        return bool(super().get(_id))
