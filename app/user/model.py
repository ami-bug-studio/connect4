from flask_security import UserMixin
from sqlalchemy import Integer, String, Boolean, Column, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref

from app.database import Base


class User(Base.Model, UserMixin):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    login = Column(String(15), unique=True, nullable=False)
    password = Column(String(128), unique=False, nullable=False)
    email = Column(String(30), unique=True, nullable=False)  # 256+ by standard

    rating_points = Column(Integer, unique=False, nullable=True, default=1000)

    confirmed_at = Column(DateTime, nullable=True)
    active = Column(Boolean, unique=False, nullable=False, default=True)

    roles = relationship(
        "Role",
        secondary="roles_users",
        backref=backref("users", lazy="joined"),
        lazy="joined",
    )

    def __str__(self):
        return f"User {self.id}: {self.login}"


class RolesUsers(Base.Model):
    __tablename__ = "roles_users"

    id = Column(Integer, primary_key=True)
    user_id = Column("user_id", Integer, ForeignKey("users.id"))
    role_id = Column("role_id", Integer, ForeignKey("roles.id"))
