from dataclasses import dataclass
from typing import Union, TypeVar

from dataclasses_json import dataclass_json
from flask_jwt_extended import create_access_token
from flask_security.recoverable import send_reset_password_instructions
from flask_security.utils import verify_password
from flask_security.views import register_user
from flask_wtf import FlaskForm
from werkzeug.datastructures import MultiDict
from wtforms import StringField, ValidationError
from wtforms.validators import DataRequired, Email, Length

from app.database import session
from app.exceptions import (
    AuthenticationException,
    RegistrationException,
    RegistrationFormValidationError,
    UnconfirmedUserException,
    UserIsntExistError,
)
from app.role.repository import RoleRepository
from app.user.repository import UserRepository
from app.user.model import User

AccessToken = TypeVar("AccessToken")


class RegistrationForm(FlaskForm):
    class Meta:
        csrf = False

    login = StringField(
        "login",
        validators=[DataRequired("Empty login"), Length(3, 15, "Bad login length")],
    )
    password = StringField(
        "password",
        validators=[
            DataRequired("Empty password"),
            Length(8, 32, "Bad password length"),
        ],
    )
    email = StringField(
        "email", validators=[DataRequired("Empty email"), Email("Incorrect email")]
    )

    def validate_login(self, field):
        if not (
            field.data.isascii()
            and all(char.isalnum() or char == "_" for char in field.data)
        ):
            raise ValidationError("Login includes insufficient characters")

    def validate_password(self, field):
        if not field.data.isascii() and field.data.isprintable():
            raise ValidationError("Password includes insufficient characters")


@dataclass_json
@dataclass
class UserAuthDTO:
    id: int
    login: str
    password: str
    email: str
    roles: [str]

    def __str__(self):
        return (
            f"UserAuthDTO: id={self.id}, login={self.login}, "
            f"email={self.email}, roles={self.roles}"
        )


@dataclass_json
@dataclass
class UserInfoDTO:
    id: int
    login: str
    confirmed_at: str
    is_active: bool
    rating_points: int

    def __str__(self):
        return (
            f"UserPlayDTO: id={self.id}, login={self.login}, is_active"
            f"={self.is_active}, rating_points={self.rating_points}"
        )


class UserService:
    @classmethod
    def authenticate_user(cls, login, password) -> Union["AccessToken", "UserAuthDTO"]:
        user = UserRepository.get_by_login(login)
        if user and verify_password(password, user.password):
            if not user.confirmed_at:
                raise UnconfirmedUserException

            return create_access_token(identity=user.id), cls.to_info_dto(user)

        raise AuthenticationException

    @classmethod
    def password_restore(cls, email=None, login=None):
        if bool(email) == bool(login):
            raise TypeError("Should be 1 argument.")

        if email:
            user = UserRepository.get_by_email(email)
        else:
            user = UserRepository.get_by_login(login)

        if not user:
            raise UserIsntExistError("No such user")

        send_reset_password_instructions(user)

    @classmethod
    def register_user(cls, email, login, password, role_name="player"):
        form_data = MultiDict(dict(email=email, login=login, password=password))

        form = RegistrationForm(form_data)
        form.validate()
        errors = form.errors

        if UserRepository.get_by_login(form.login.data):
            errors.setdefault("login", []).append("Login isn't unique")

        if UserRepository.get_by_email(form.email.data):
            errors.setdefault("email", []).append("Email isn't unique")

        if errors:
            raise RegistrationFormValidationError(errors=errors)

        role = RoleRepository.get_by_name(role_name)
        if not role:
            raise RegistrationException(f"No such role as <{role_name}>.")
        try:
            user = register_user(
                email=email, login=login, password=password, roles=[role]
            )
        except Exception as exc:
            session.rollback()
            raise RegistrationException(
                "Error during <flask-security register_user()>"
            ) from exc

        return UserService.to_info_dto(user)

    @classmethod
    def get_by_id(cls, id_: int):
        """Get user info by login"""

        if isinstance(id_, int):
            return UserRepository.get(id_)

    @classmethod
    def from_auth_dto(cls, user: UserAuthDTO) -> User:
        if user is not None:
            roles = []
            if user.roles is not None and len(user.roles) > 0:
                for role in user.roles:
                    roles.append(RoleRepository.get_by_name(name=role))
            return User(
                id=user.id,
                login=user.login,
                password=user.password,
                email=user.email,
                roles=roles,
            )

    @classmethod
    def to_info_dto(cls, user: User) -> UserInfoDTO:
        if user is not None:
            return UserInfoDTO(
                id=user.id,
                login=user.login,
                confirmed_at=str(user.confirmed_at),
                is_active=user.is_active,
                rating_points=user.rating_points,
            )

    @classmethod
    def to_auth_dto(cls, user: User) -> UserAuthDTO:
        if user is not None:
            return UserAuthDTO(
                id=user.id,
                login=user.login,
                password=user.password,
                email=user.email,
                roles=[role.name for role in user.roles],
            )
