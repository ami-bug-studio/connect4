from flask_babel import Babel
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_security import Security
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import scoped_session
from flask_marshmallow import Marshmallow

migrate = Migrate()

Base = SQLAlchemy()
session: scoped_session = Base.session
jwt = JWTManager()

security = Security()  # Don't use before create_app finishing!

mail = Mail()

babel = Babel(default_locale="eng")

marshmallow = Marshmallow()
