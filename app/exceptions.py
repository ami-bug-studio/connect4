from sqlalchemy.exc import SQLAlchemyError

# TODO: group exceptions by origin type


class UserServiceException(Exception):
    pass


class AuthenticationException(UserServiceException):
    pass


class RegistrationException(UserServiceException):
    pass


class UnconfirmedUserException(AuthenticationException):
    pass


class RegistrationFormValidationError(RegistrationException):
    def __init__(self, *args, errors, **kwargs):
        self.errors = errors
        super().__init__(*args, **kwargs)


class UserIsntExistError(Exception):
    pass


class AccessException(Exception):
    pass


class MatchServiceException(Exception):
    pass


class MatchServiceAccessException(AccessException, MatchServiceException):
    pass


class MatchCfgException(MatchServiceException):
    pass


class PlayerAssignationException(MatchServiceException):
    pass


class SinglePlayServiceException(MatchServiceException):
    pass


class RepositorySQLAlchemyError(SQLAlchemyError):
    pass


class PlayerServiceException(Exception):
    pass


class InstanceNotFoundException(Exception):
    pass


class PlayerServiceAccessException(AccessException, PlayerServiceException):
    pass
