"""Main app config.

Make your own copy during development.

https://pythonhosted.org/Flask-Security/configuration.html - more about flask
security cvars"""

import os


class Config:
    CSRF_ENABLED = True
    # WTF_CSRF_ENABLED = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_DATABASE_URI = property(lambda self:
                                       os.environ['DATABASE_URL'])

    SECURITY_RECOVERABLE = True
    SECURITY_CONFIRMABLE = True
    SECURITY_CHANGEABLE = False

    # do always pluralize the time unit
    SECURITY_CONFIRM_EMAIL_WITHIN = '3 days'
    SECURITY_RESET_PASSWORD_WITHIN = '60 minutes'

    SECURITY_RESET_URL = '/reset'
    SECURITY_POST_RESET_VIEW = '/password_reset_finished'
    SECURITY_POST_CONFIRM_VIEW = '/email_confirmation_finished'
    SECURITY_UNAUTHORIZED_VIEW = None
    # SECURITY_LOGIN_URL = '/api/auth/login'
    # SECURITY_CONFIRM_URL = '/api/auth/confirm'
    # SECURITY_CONFIRM_ERROR_VIEW = '/greetings_at_confirmation'
    # SECURITY_POST_CHANGE_VIEW = '/greetings_at_confirmation'
    # SECURITY_UNAUTHORIZED_VIEW = 'Auth_rest.unauthorized'


    # SECURITY_EMAIL_SUBJECT_REGISTER = 'Welcome to the Connect4 Game!'
    SECURITY_EMAIL_PLAINTEXT = True
    SECURITY_EMAIL_HTML = True

    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_SERVER = property(lambda self:
                           os.environ['MAIL_SERVER'])
    MAIL_PORT = property(lambda self:
                         os.environ['MAIL_PORT'])
    MAIL_USERNAME = property(lambda self:
                             os.environ['MAIL_USERNAME'])
    MAIL_PASSWORD = property(lambda self:
                             os.environ['MAIL_PASSWORD'])
    SECURITY_EMAIL_SENDER = property(lambda self:
                                     os.environ['SECURITY_EMAIL_SENDER'])
    # MAIL_DEFAULT_SENDER

    BABEL_DEFAULT_LOCALE = 'ru_RU'


class ProductionConfig(Config):
    DEVELOPMENT = False
    DEBUG = False

    SECRET_KEY = property(lambda self:
                          os.environ['APP_SECRET_KEY'])

    SECURITY_PASSWORD_SALT = property(lambda self:
                                      os.environ['SECURITY_PASSWORD_SALT'])
    SECURITY_CONFIRM_SALT = property(lambda self:
                                     os.environ['SECURITY_CONFIRM_SALT'])
    SECURITY_RESET_SALT = property(lambda self:
                                   os.environ['SECURITY_RESET_SALT'])


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SECRET_KEY = 'moar secrets'

    SQLALCHEMY_ECHO = False

    MAIL_FAIL_SILENTLY = False

    SECURITY_PASSWORD_SALT = 'mongolians drink tea with salt'
    SECURITY_CONFIRM_SALT = 'very secret confirm salt'
    SECURITY_RESET_SALT = 'who cares'

    MAIL_SERVER = ''
    MAIL_PORT = 25
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    SECURITY_EMAIL_SENDER = 'connect5781@bratanmail.doom'


class TestingConfig(DevelopmentConfig):
    TESTING = True
    MAIL_SUPPRESS_SEND = True
