"""empty message

Revision ID: 806c4716fb9d
Revises: 7f85e86848f0
Create Date: 2020-04-26 20:19:20.677709

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '806c4716fb9d'
down_revision = '7f85e86848f0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('matches',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('completionDate', sa.DateTime(), nullable=True),
    sa.Column('difficulty', sa.Integer(), nullable=False),
    sa.Column('fieldHeight', sa.Integer(), nullable=False),
    sa.Column('fieldWidth', sa.Integer(), nullable=False),
    sa.Column('startDate', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('players',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('is_ai', sa.Boolean(), nullable=True),
    sa.Column('is_winner', sa.Boolean(), nullable=True),
    sa.Column('score', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('match_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['match_id'], ['matches.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('players')
    op.drop_table('matches')
    # ### end Alembic commands ###
