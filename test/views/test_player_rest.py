import json
import typing
from datetime import datetime
from unittest import TestCase

from flask import url_for
from flask_security.registerable import hash_password

from app.database import Base, session
from app.match.model import Match
from app.match.repository import MatchRepository
from app.match.schema import MatchSchema
from app.match.service import MatchService
from app.player.repository import PlayerRepository
from app.player.schema import PlayerSchema
from app.role.model import Role
from app.user.model import User
from manage import add_initial_player_role, app
from test.user import user_dict_generator


class PlayerRestTest(TestCase):
    user_generator = user_dict_generator(active=True)

    def setUp(self):
        self.ctx = app.test_request_context()
        self.ctx.push()

        self.client = app.test_client()

        Base.drop_all()
        Base.create_all()
        add_initial_player_role()

        self.role = session.query(Role).first()

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = User(**user_dict)
        session.add(self.user)

        user_dict_2 = next(self.user_generator)
        user_dict_2["roles"] = [self.role]
        self.user_2 = User(**user_dict_2)
        session.add(self.user_2)

        session.commit()

        self.player_schema = PlayerSchema()
        self.match_schema = MatchSchema(
            load_only=("id", "players", "startDate", "completionDate")
        )

    def tearDown(self) -> None:
        session.close()
        self.ctx.pop()

    def add_user(self) -> typing.Tuple[User, str]:
        user_dict = next(self.user_generator)

        password = user_dict["password"]
        user_dict["password"] = hash_password(user_dict["password"])
        user_dict["confirmed_at"] = datetime.utcnow()

        user_dict["roles"] = [self.role]

        user = User(**user_dict)

        session.add(user)
        session.commit()

        return user, password

    def create_match(self, **params):
        match_field_size = MatchService.Defaults.field_sizes[0]
        self.match_default_params = Match(**{
            **dict(
                fieldWidth=match_field_size[0],
                fieldHeight=match_field_size[1],
                difficulty=1,
                startDate=datetime.utcnow()
            ),
            **params,
        })

        match = MatchRepository.create(self.match_default_params)
        return match

    def create_players(self, match, users=None):
        if not users:
            users = [self.user, self.user_2]
        if len(users) == 1:
            users.append(self.user)

        player_1 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user=users[0],
            match=match
        )

        player_2 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user=users[1],
            match=match
        )

        return player_1, player_2

    def get_headers(self, auth=True, user=None, password=None):
        headers = {"Content-Type": "application/json"}

        if auth:
            if not user:
                user, password = self.add_user()

            data = json.dumps(dict(login=user.login, password=password))

            response = self.client.post(
                "/api/auth/login", data=data, content_type="application/json"
            )
            headers["Authorization"] = f"Bearer {response.json['access_token']}"

        return headers

    def test_player_get(self):
        user, password = self.add_user()

        match = MatchService._create(self.create_match())
        self.create_players(match, users=[user])

        response = self.client.get(
            url_for("Player_rest.player", player_id=match.players[0].id),
            headers=self.get_headers(user=user, password=password),
        )

        self.assertEqual(200, response.status_code, response.json)

    def test_player_get_nonexistent(self):
        user, password = self.add_user()
        MatchService._create(self.create_match())

        response = self.client.get(
            url_for("Player_rest.player", player_id=123321),
            headers=self.get_headers(user=user, password=password),
        )

        self.assertEqual(404, response.status_code, response.json)

    def test_match_get_without_access(self):
        user, password = self.add_user()
        match = MatchService._create(self.create_match())
        self.create_players(match, users=[user])

        response = self.client.get(
            url_for("Player_rest.player", player_id=match.players[0].id),
            headers=self.get_headers(),
        )

        self.assertEqual(401, response.status_code, response.json)

    def test_match_get_without_auth(self):
        response = self.client.get(
            url_for("Player_rest.player", player_id=123),
            headers=self.get_headers(auth=False),
        )

        self.assertEqual(401, response.status_code, response.json)
