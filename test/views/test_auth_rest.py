import datetime
import json
from unittest import TestCase

from flask_security.registerable import hash_password

from app.database import Base, session
from app.role.model import Role as RoleModel
from app.user.model import User as UserModel
from manage import app, add_initial_player_role
from test.user import user_dict_generator


class AuthRestTest(TestCase):
    user_generator = user_dict_generator(active=True)

    def setUp(self):
        self.ctx = app.test_request_context()
        self.ctx.push()

        self.client = app.test_client()

        Base.drop_all()
        Base.create_all()
        add_initial_player_role()

        self.user_dict = next(self.user_generator)

        self.role = session.query(RoleModel).first()
        self.user_dict["roles"] = [self.role]

        self.password = self.user_dict["password"]
        self.user_dict["password"] = hash_password(self.user_dict["password"])
        self.user_dict["confirmed_at"] = datetime.datetime.now()

    def tearDown(self) -> None:
        self.ctx.pop()
        session.close()

    def register_user(self):
        session.add(UserModel(**self.user_dict))
        session.commit()

    # ##
    # ## login tests ## #
    # ##

    def test_normal_login(self):
        self.register_user()

        data = json.dumps(dict(login=self.user_dict["login"], password=self.password))

        response = self.client.post(
            "/api/auth/login", data=data, content_type="application/json"
        )

        self.assertEqual(200, response.status_code)

    def test_login_of_unconfirmed_user(self):
        del self.user_dict["confirmed_at"]
        self.register_user()

        data = json.dumps(dict(login=self.user_dict["login"], password=self.password))

        response = self.client.post(
            "/api/auth/login", data=data, content_type="application/json"
        )

        self.assertEqual(401, response.status_code)

    def test_login_of_nonexistent_user(self):
        data = json.dumps(dict(login="nobody", password="whatever"))

        response = self.client.post(
            "/api/auth/login", data=data, content_type="application/json"
        )

        self.assertEqual(401, response.status_code)

    # ##
    # ## registration tests ## #
    # ##

    def test_normal_registration(self):
        data = json.dumps(
            dict(
                login=self.user_dict["login"],
                password=self.password,
                email=self.user_dict["email"],
            )
        )

        response = self.client.post(
            "/api/auth/register", data=data, content_type="application/json"
        )

        self.assertEqual(202, response.status_code)

    def test_registration_with_invalid_credentials(self):
        data = json.dumps(
            dict(
                login=self.user_dict["login"] * 100,
                password=self.password,
                email=self.user_dict["email"],
            )
        )

        response = self.client.post(
            "/api/auth/register", data=data, content_type="application/json"
        )

        self.assertEqual(400, response.status_code)

    # ##
    # ## password restoring tests ## #
    # ##

    def test_password_restore(self):
        self.register_user()

        data = json.dumps(dict(login=self.user_dict["login"]))
        response = self.client.post(
            "/api/auth/password_restore", data=data, content_type="application/json"
        )

        self.assertEqual(200, response.status_code)

        data = json.dumps(dict(email=self.user_dict["email"]))
        response = self.client.post(
            "/api/auth/password_restore", data=data, content_type="application/json"
        )

        self.assertEqual(200, response.status_code)

    def test_password_restore_with_two_arguments(self):
        self.register_user()

        data = json.dumps(
            dict(login=self.user_dict["login"], email=self.user_dict["email"])
        )
        response = self.client.post(
            "/api/auth/password_restore", data=data, content_type="application/json"
        )

        self.assertEqual(400, response.status_code)

    def test_password_restore_with_nonexistent_user(self):
        data = json.dumps(dict(login="nobody"))
        response = self.client.post(
            "/api/auth/password_restore", data=data, content_type="application/json"
        )

        self.assertEqual(400, response.status_code)

        data = json.dumps(dict(email="nobody@noservice.eh"))
        response = self.client.post(
            "/api/auth/password_restore", data=data, content_type="application/json"
        )

        self.assertEqual(400, response.status_code)

    #
    # misc
    #

    def test_json_required(self):
        response = self.client.post(
            "/api/auth/password_restore", content_type="program/jason"
        )
        self.assertEqual(406, response.status_code)
