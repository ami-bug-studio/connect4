import unittest
from datetime import datetime

from app import Base, Match, session
from app.exceptions import SinglePlayServiceException
from app.game.service import SinglePlayService
from app.match.repository import MatchRepository
from app.match.schema import MatchSchema
from app.match.service import MatchService
from app.player.schema import PlayerSchema
from app.role.model import Role
from app.user.model import User
from manage import app
from test.user import user_dict_generator


class SinglePlayServiceTest(unittest.TestCase):
    user_generator = user_dict_generator()

    def setUp(self) -> None:
        app.test_request_context().push()

        Base.drop_all()
        Base.create_all()

        self.role = Role(name="player", description="Simple player entity")
        session.add(self.role)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = User(**user_dict)
        session.add(self.user)

        session.commit()

        self.player_schema = PlayerSchema()
        self.match_schema = MatchSchema()

    def create_match(self, **params) -> Match:
        match_field_size = MatchService.Defaults.field_sizes[0]
        return Match(**{
            **dict(
                fieldWidth=match_field_size[0],
                fieldHeight=match_field_size[1],
                difficulty=1,
                startDate=datetime.now(),
            ),
            **params,
        })

    def tearDown(self) -> None:
        session.close()


