import datetime
import re
import time
from unittest import TestCase
from unittest.mock import Mock

from flask.testing import FlaskClient
from flask_security.registerable import hash_password

from app.database import Base, session, mail
from app.role.model import Role as RoleModel
from app.user.model import User as UserModel
from app.user.repository import UserRepository
from app.user.service import UserService
from app.exceptions import (
    AuthenticationException,
    RegistrationException,
    RegistrationFormValidationError,
    UnconfirmedUserException,
    UserIsntExistError,
)
from manage import app
from test.user import user_dict_generator


class TestUserService(TestCase):
    user_generator = user_dict_generator(active=True)

    def setUp(self) -> None:
        app.test_request_context().push()

        self.client: FlaskClient = app.test_client()

        mail.send = Mock()

        Base.drop_all()
        Base.create_all()
        self.assertEqual(session.query(UserModel).count(), 0)
        self.assertEqual(session.query(RoleModel).count(), 0)

        session.add(RoleModel(name="player", description="Simple player entity"))
        session.commit()

        self.role = session.query(RoleModel).first()

        self.user_dict = next(self.user_generator)
        self.user_dict["roles"] = [self.role]
        self.password = self.user_dict["password"]
        self.user_dict["password"] = hash_password(self.user_dict["password"])
        self.user_dict["confirmed_at"] = datetime.datetime.now()

        session.add(UserModel(**self.user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 1)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_authenticate_user(self):
        access_token, user = UserService.authenticate_user(
            self.user_dict["login"], self.password
        )

        self.assertIsNotNone(access_token)
        self.assertEqual(user.login, self.user_dict["login"])

    def test_dont_authenticate_nonexistent_user(self):
        with self.assertRaises(AuthenticationException):
            UserService.authenticate_user("nonexistentlogin", "randompw")

    def test_dont_authenticate_unconfirmed_user(self):
        user = next(self.user_generator)

        UserService.register_user(user["email"], user["login"], user["password"])

        with self.assertRaises(UnconfirmedUserException):
            UserService.authenticate_user(user["login"], user["password"])

    def test_dont_register_user_with_incorrect_credentials(self):
        # Too short and with insufficient characters.
        login = "ЯЗ"
        password = "ЬЯ!"
        email = "notanemail@"

        with self.assertRaises(RegistrationFormValidationError) as exc:
            UserService.register_user(email, login, password)

        self.assertEqual(
            5, sum(len(exc.exception.errors[key]) for key in
                   exc.exception.errors),
            msg=exc.exception.errors
        )

        login = "ok_login" * 20
        password = "Voldemort" * 20
        email = "mail@santa.ll"

        with self.assertRaises(RegistrationFormValidationError, msg="Too long") as exc:
            UserService.register_user(email, login, password)

        self.assertEqual(
            2, sum(len(exc.exception.errors[key]) for key in exc.exception.errors)
        )

    def test_dont_register_with_nonunique_login_and_mail(self):
        user = next(self.user_generator)

        UserService.register_user(user["email"], user["login"], user["password"])

        with self.assertRaises(RegistrationFormValidationError) as exc:
            UserService.register_user(user["email"], user["login"], user["password"])

        self.assertEqual(
            2, sum(len(exc.exception.errors[key]) for key in exc.exception.errors)
        )

    def test_dont_register_with_bad_role(self):
        user = next(self.user_generator)

        with self.assertRaises(RegistrationException):
            UserService.register_user(
                user["email"], user["login"], user["password"], role_name="nonexistent"
            )

    def test_register_user_with_sending_email(self):
        user = next(self.user_generator)

        self.assertEqual(session.query(UserModel).count(), 1)

        user_info_dto = UserService.register_user(
            user["email"], user["login"], user["password"]
        )
        time.sleep(0)

        send_mail = mail.send.call_args[0][0]
        self.assertIn(user["email"], send_mail.body)
        self.assertIn("a href", send_mail.html)

        self.assertEqual(session.query(UserModel).count(), 2)
        self.assertIsNotNone(UserRepository.get_by_login(user["login"]))
        self.assertEqual(user["login"], user_info_dto.login)

    def test_registration_email_confirmation(self):
        user = next(self.user_generator)

        UserService.register_user(user["email"], user["login"], user["password"])
        time.sleep(0)

        send_mail = mail.send.call_args[0][0]
        self.assertIsNone(UserRepository.get_by_login(user["login"]).confirmed_at)

        confirm_link = re.search(r"(https?://[^\s]+)", send_mail.body).group(0)
        response = self.client.get(confirm_link)

        self.assertEqual(response.status_code, 302)
        self.assertIsNotNone(UserRepository.get_by_login(user["login"]).confirmed_at)

    def test_password_restore(self):
        UserService.password_restore(email=self.user_dict["email"])
        time.sleep(0)

        send_mail = mail.send.call_args[0][0]

        self.assertIn(self.user_dict["email"], send_mail.recipients)

        user = next(self.user_generator)

        UserService.register_user(user["email"], user["login"], user["password"])

        UserService.password_restore(login=user["login"])
        time.sleep(0)

        send_mail = mail.send.call_args[0][0]
        self.assertIn(user["email"], send_mail.recipients)

        restore_link = re.search(r"(https?://[^\s]+)", send_mail.body).group(0)
        response = self.client.get(restore_link)

        self.assertEqual(response.status_code, 200)

    def test_dont_restore_password_for_nonexistent_user(self):
        with self.assertRaises(UserIsntExistError):
            UserService.password_restore(email="no_such_email@dao.ez")
