import unittest

from app.database import session, Base
from app.role.model import Role as RoleModel
from app.user.model import User as UserModel
from app.user.repository import UserRepository
from manage import app
from test.user import user_dict_generator


class UserRepositoryTest(unittest.TestCase):
    user_generator = user_dict_generator()

    def setUp(self):
        app.test_request_context().push()
        Base.drop_all()
        Base.create_all()

        session.add(RoleModel(name="player", description="Simple player entity"))
        self.role = session.query(RoleModel).first()

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]

        session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 1)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def tearDown(self) -> None:
        session.close()

    def test_save(self):
        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        user = UserRepository.save(UserModel(**user_dict))

        self.assertTrue(user.id > 0)
        self.assertEqual(session.query(UserModel).count(), 2)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def test_save_by_attrs(self):
        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        user = UserRepository.save_by_attrs(**user_dict)

        self.assertTrue(user.id > 0)
        self.assertEqual(session.query(UserModel).count(), 2)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def test_save_all(self):
        users = []
        for user in range(10):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            users.append(UserModel(**user_dict))
        _users = UserRepository.save_all(users)

        self.assertTrue(len(_users) == 10)
        self.assertEqual(session.query(UserModel).count(), 11)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def test_delete(self):
        user = session.query(UserModel).first()

        UserRepository.delete(user)

        self.assertEqual(session.query(UserModel).count(), 0)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def test_delete_all(self):

        for _users in range(10):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 11)
        self.assertEqual(session.query(RoleModel).count(), 1)
        _users = session.query(UserModel).all()

        UserRepository.delete_all(_users)

        self.assertEqual(session.query(UserModel).count(), 0)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def test_get_by_id(self):
        test_user = session.query(UserModel).first()

        user = UserRepository.get_by_id(test_user.id)

        self.assertIsNotNone(user)
        self.assertTrue(user.id == test_user.id)

        user = UserRepository.get_by_id(8500)

        self.assertIsNone(user)

    def test_get_by_login(self):
        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 3)
        self.assertEqual(session.query(RoleModel).count(), 1)
        test_user = session.query(UserModel).all()[2]

        user = UserRepository.get_by_login(test_user.login)
        self.assertIsNotNone(user)
        self.assertTrue(user.id == test_user.id)
        self.assertTrue(user.login == test_user.login)

        user = UserRepository.get_by_login("UserX")
        self.assertIsNone(user)

    def test_get_by_email(self):

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 3)
        self.assertEqual(session.query(RoleModel).count(), 1)
        test_user = session.query(UserModel).all()[1]

        user = UserRepository.get_by_email(test_user.email)

        self.assertIsNotNone(user)
        self.assertTrue(user.id == test_user.id)
        self.assertTrue(user.email == test_user.email)

        user = UserRepository.get_by_email("Xemail@mail.com")
        self.assertIsNone(user)

    def test_get_all_active(self):

        user_dict = next(self.user_generator)
        user_dict["active"] = True
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["active"] = True
        user_dict["roles"] = [self.role]
        test_login = user_dict["login"]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        user_dict["active"] = False
        session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 4)
        self.assertEqual(session.query(RoleModel).count(), 1)

        users = UserRepository.get_all_active()

        self.assertEqual(len(users), 2)
        self.assertTrue(users[0].is_active)
        self.assertIn(test_login, (user.login for user in users))

    def test_get_all_active_pageable(self):

        for item in range(15):
            user_dict = next(self.user_generator)
            user_dict["active"] = True
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        for item in range(5):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 21)
        self.assertEqual(session.query(RoleModel).count(), 1)

        users = UserRepository.get_all_active_pageable(dict(id="desc"), 1, 10)

        self.assertTrue(users.total == 15)
        self.assertTrue(len(users.items) == 10)
        prev_id = 100
        for user in users.items:
            self.assertTrue(user.is_active)
            self.assertTrue(user.id < prev_id)
            prev_id = user.id

        users = UserRepository.get_all_active_pageable(
            dict(id="asc", login="desc"), 2, 10
        )

        self.assertTrue(users.total == 15)
        self.assertTrue(len(users.items) == 5)
        prev_id = -1
        prev_login_id = -1
        for user in users.items:
            self.assertTrue(user.is_active)
            self.assertTrue(user.id > prev_id)
            self.assertTrue(int(user.login[4:]) > prev_login_id)
            prev_id = user.id
            prev_login_id = int(user.login[4:])

        users = UserRepository.get_all_active_pageable(dict(), 3, 10)

        self.assertTrue(users.total == 15)
        self.assertTrue(len(users.items) == 0)

    def test_find_all_by_login(self):

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        session.commit()
        self.assertEqual(session.query(UserModel).count(), 3)
        self.assertEqual(session.query(RoleModel).count(), 1)
        test_user = session.query(UserModel).get(2)

        users = UserRepository.find_all_by_login("us")

        self.assertTrue(len(users) == 3)

        users = UserRepository.find_all_by_login(test_user.login)

        self.assertTrue(len(users) == 1)
        self.assertTrue(users[0].id == test_user.id)

        users = UserRepository.find_all_by_login("xUs")
        self.assertTrue(len(users) == 0)

    def test_find_all_by_login_pageable(self):

        for item in range(20):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 21)
        self.assertEqual(session.query(RoleModel).count(), 1)

        users = UserRepository.find_all_by_login_pageable(
            "us", dict(login="asc"), 1, 10
        )

        self.assertTrue(users.total == 21)
        self.assertTrue(len(users.items) == 10)
        prev_login_id = -1
        for user in users.items:
            self.assertTrue(int(user.login[4:]) > prev_login_id)
            prev_login_id = int(user.login[4:])

        users = UserRepository.find_all_by_login_pageable(
            "User", dict(login="desc"), 1, 5
        )

        self.assertTrue(users.total == 21)
        self.assertTrue(len(users.items) == 5)
        prev_login_id = 1000
        for user in users.items:
            self.assertTrue(int(user.login[4:]) < prev_login_id)
            prev_login_id = int(user.login[4:])

        users = UserRepository.find_all_by_login_pageable("usx", dict(), 1, 10)

        self.assertTrue(users.total == 0)
        self.assertTrue(len(users.items) == 0)

    def test_find_all_by_email(self):

        for item in range(20):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 21)
        self.assertEqual(session.query(RoleModel).count(), 1)

        users = UserRepository.find_all_by_email("chu")
        self.assertTrue(len(users) == 21)

        test_users = (
            session.query(UserModel).filter(UserModel.email.ilike("chupapro1%")).all()
        )

        users = UserRepository.find_all_by_email("chupapro")
        self.assertTrue(len(users) == 21)

        users = UserRepository.find_all_by_email("chupapro1")
        self.assertTrue(len(users) == len(test_users))

    def test_find_all_by_email_pageable(self):

        for item in range(20):
            user_dict = next(self.user_generator)
            user_dict["roles"] = [self.role]
            session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 21)
        self.assertEqual(session.query(RoleModel).count(), 1)

        users = UserRepository.find_all_by_email_pageable(
            "chupa", dict(email="asc"), 1, 10
        )
        self.assertTrue(users.total == 21)
        self.assertTrue(len(users.items) == 10)

        prev_email_id = -1
        for user in users.items:
            self.assertTrue(int(user.email[8: len(user.email) - 10]) > prev_email_id)
            prev_email_id = int(user.email[8: len(user.email) - 10])

        test_users = (
            session.query(UserModel).filter(UserModel.email.ilike("chupapro1%")).all()
        )

        users = UserRepository.find_all_by_email_pageable(
            "chupapro1", dict(email="desc"), 1, 5
        )

        self.assertTrue(users.total == len(test_users))
        self.assertEqual(
            len(users.items), 5 if len(test_users) > 5 else len(test_users)
        )

        prev_email_id = 1000
        for user in users.items:
            self.assertTrue(int(user.email[8: len(user.email) - 10]) < prev_email_id)
            prev_email_id = int(user.email[8: len(user.email) - 10])

        users = UserRepository.find_all_by_email_pageable("email", dict(), 1, 10)
        self.assertTrue(users.total == 0)
        self.assertTrue(len(users.items) == 0)

    def test_exists(self):

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))

        self.assertEqual(session.query(UserModel).count(), 3)
        self.assertEqual(session.query(RoleModel).count(), 1)
        test_user = session.query(UserModel).first()

        self.assertTrue(UserRepository.exists(test_user.id))
        self.assertFalse(UserRepository.exists(520))

    def test_dunder(self):
        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]

        user = UserModel(**user_dict)
        str(user)
