def user_dict_generator(*removed_default_args, **additional_kwargs):
    rand_idx = 0

    while True:
        user = dict(
            login=f"User{rand_idx}",
            password="ABCD" * 5,  # 128
            email=f"chupapro{rand_idx}@gmail.com",
            active=False,
            rating_points=1000,
        )

        for key in removed_default_args:
            del user[key]

        user = {**user, **additional_kwargs}

        yield user
        rand_idx += 1
