import datetime
import unittest

import sqlalchemy

from app.database import Base, session
from app.match.model import Match
from app.player.model import Player
from app.player.repository import PlayerRepository
from app.role.model import Role as RoleModel
from app.user.model import User as UserModel
from manage import app
from test.user import user_dict_generator


class PlayerRepositoryTest(unittest.TestCase):
    user_generator = user_dict_generator()

    def setUp(self):
        app.test_request_context().push()
        Base.drop_all()
        Base.create_all()

        self.role = RoleModel(name="player", description="Simple player entity")
        session.add(self.role)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = UserModel(**user_dict)
        session.add(self.user)

        self.match = Match(
            fieldHeight=5, fieldWidth=6, difficulty=1, startDate=datetime.datetime.now()
        )
        session.add(self.match)

        session.commit()

        self.assertEqual(session.query(UserModel).count(), 1)
        self.assertEqual(session.query(RoleModel).count(), 1)
        self.assertEqual(session.query(Match).count(), 1)

    def tearDown(self):
        session.close()

    def test_get_by_id(self):
        player = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        player.match = self.match

        session.add(player)
        session.commit()

        test_player = PlayerRepository.get(player.id)

        self.assertEqual(player, test_player)

    def test_get_by_match_id(self):
        player = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        player.match = self.match
        session.add(player)

        match_2 = Match(
            fieldHeight=5, fieldWidth=6, difficulty=1, startDate=datetime.datetime.now()
        )
        player_2 = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        session.add(player_2)
        session.add(match_2)

        player_2.match = match_2

        session.commit()

        self.assertEqual(player, PlayerRepository.get_by_match(self.match.id)[0])
        self.assertEqual(player_2, PlayerRepository.get_by_match(match_2.id)[0])

    def test_get_by_user_id(self):
        player = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        player.match = self.match
        player.user = self.user
        session.add(player)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        user_2 = UserModel(**user_dict)
        session.add(user_2)

        player_2 = Player(is_ai=False, is_winner=False, score=0, user_id=user_2.id)
        match_2 = Match(
            fieldHeight=5, fieldWidth=6, difficulty=1, startDate=datetime.datetime.now()
        )
        session.add(match_2)
        player_2.match = match_2
        player_2.user = user_2
        session.add(player_2)

        session.commit()

        self.assertEqual(
            player, PlayerRepository.get_all_by_field("user_id", self.user.id)[0]
        )
        self.assertEqual(
            player_2, PlayerRepository.get_all_by_field("user_id", user_2.id)[0]
        )

    def test_create_with_incorrect_fields(self):
        with self.assertRaises(TypeError):
            PlayerRepository.create(
                is_ai="wrong!",
                is_winner=False,
                score=0,
                user_id=self.user.id,
                no_such_field=324,
            )

    def test_create_and_update(self):
        player = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user_id=self.user.id,
            match=self.match
        )
        session.add(player)

        session.commit()

        test_score = 777
        PlayerRepository.update(player.id, score=test_score)
        self.assertEqual(PlayerRepository.get(player.id).score, test_score)

        with self.assertRaises(KeyError):
            PlayerRepository.update(player.id, inexistent_field=234)

        with self.assertRaises(sqlalchemy.exc.DataError):
            PlayerRepository.update(player.id, score="some string")

    def test_get_by_user(self):
        player = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        player.match = self.match
        session.add(player)
        session.commit()

        self.assertEqual([player], PlayerRepository.get_by_user(self.user.id))

    def test_str_dunder(self):
        player = Player(is_ai=False, is_winner=False, score=0, user_id=self.user.id)
        str(player)
