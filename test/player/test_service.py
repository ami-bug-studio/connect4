import unittest
from datetime import datetime

from app.database import Base, session
from app.exceptions import (
    InstanceNotFoundException,
    PlayerServiceAccessException
)
from app.match.model import Match
from app.player.model import Player
from app.player.schema import PlayerSchema
from app.player.service import PlayerService
from app.role.model import Role
from app.user.model import User
from manage import app
from test.user import user_dict_generator


class PlayerRepositoryTest(unittest.TestCase):
    user_generator = user_dict_generator()

    def setUp(self) -> None:
        app.test_request_context().push()
        Base.drop_all()
        Base.create_all()

        self.role = Role(name="player", description="Simple player entity")
        session.add(self.role)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = User(**user_dict)
        session.add(self.user)

        self.match = Match(
            fieldHeight=5, fieldWidth=6, difficulty=1,
            startDate=datetime.now()
        )
        session.add(self.match)

        session.commit()
        self.player_schema = PlayerSchema()

    def tearDown(self):
        session.close()

    def test_get_player(self):
        player = Player(is_ai=False, is_winner=False, score=0, user=self.user)
        player.match = self.match
        session.add(player)
        session.commit()

        self.assertEqual(
            PlayerService.get_player(player.id, self.user.id),
            self.player_schema.dump(player),
        )

        self.assertEqual(
            PlayerService.get_player(player.id),
            self.player_schema.dump(player),
        )

        with self.assertRaises(PlayerServiceAccessException):
            PlayerService.get_player(player.id, self.user.id + 1)

        with self.assertRaises(InstanceNotFoundException):
            PlayerService.get_player(player.id + 1, self.user.id)
