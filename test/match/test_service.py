import unittest
from datetime import datetime

from app import Base, session
from app.exceptions import (
    InstanceNotFoundException, MatchCfgException,
    MatchServiceAccessException,
    MatchServiceException,
)
from app.match.model import Match
from app.match.repository import MatchRepository
from app.match.schema import MatchSchema
from app.match.service import MATCH_SCHEMA, MatchService
from app.player.repository import PlayerRepository
from app.player.schema import PlayerSchema
from app.role.model import Role
from app.user.model import User
from manage import app
from test.user import user_dict_generator


class MatchServiceTest(unittest.TestCase):
    user_generator = user_dict_generator()

    def setUp(self) -> None:
        app.test_request_context().push()

        Base.drop_all()
        Base.create_all()

        self.role = Role(name="player", description="Simple player entity")
        session.add(self.role)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = User(**user_dict)
        session.add(self.user)

        user_dict_2 = next(self.user_generator)
        user_dict_2["roles"] = [self.role]
        self.user_2 = User(**user_dict_2)
        session.add(self.user_2)

        session.commit()

        self.player_schema = PlayerSchema()
        self.match_schema = MatchSchema()

    def create_players(self, match, users=None):
        if not users:
            users = [self.user, self.user_2]
        if len(users) == 1:
            users.append(self.user)

        player_1 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user=users[0],
            match=match
        )

        player_2 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user=users[1],
            match=match
        )

        return player_1, player_2

    def create_match(self, **params):
        match_field_size = MatchService.Defaults.field_sizes[0]
        return Match(**{
            **dict(
                fieldWidth=match_field_size[0],
                fieldHeight=match_field_size[1],
                difficulty=1,
                startDate=datetime.now(),
            ),
            **params,
        })

    def tearDown(self):
        session.close()

    def test_create_match(self):
        match = self.create_match()
        self.assertIsInstance(MatchService._create(match), Match)

    def test_create_singleplayer_match_raises_on_bad_field_size(self):
        match = self.create_match(fieldHeight=13377331, players=[])

        with self.assertRaises(MatchCfgException):
            MatchService.create_singleplayer(match, self.user.id)

        match_2 = self.create_match(fieldWidth=13377331, players=[])
        with self.assertRaises(MatchCfgException):
            MatchService.create_singleplayer(match_2, self.user.id)

        match_3 = self.create_match(difficulty=13377331, players=[])
        with self.assertRaises(MatchCfgException):
            MatchService.create_singleplayer(match_3, self.user.id)

    def test_start(self):
        match = MatchService._create(self.create_match(startDate=None))
        self.create_players(match)

        MatchService.start(match.id)

        self.assertIsNotNone(match.startDate)

        with self.assertRaises(MatchServiceException):
            MatchService.start(match.id)

    def test_is_user_still_playing_check(self):
        match = MatchService._create(self.create_match(startDate=None))
        self.create_players(match)

        self.assertFalse(MatchService.is_user_playing(self.user.id))

        MatchRepository.update(match.id, startDate=datetime.utcnow())

        self.assertTrue(MatchService.is_user_playing(self.user.id))

    def test_get_current(self):
        self.assertIsNone(MatchService.get_current(self.user.id))

        match = MatchService._create(self.create_match())
        self.create_players(match)

        self.assertEqual(MatchService.get_current(self.user.id), match)

    def test_delete_unbegun(self):
        match = MatchService._create(self.create_match())

        with self.assertRaises(MatchServiceException):
            MatchService.delete_unbegun(match.id, self.user.id)

        self.assertIsNotNone(session.query(Match).get(match.id))

        match_2 = MatchService._create(self.create_match(startDate=None))
        self.create_players(match_2)

        MatchService.delete_unbegun(match_2.id, self.user.id)

        # Just in case
        self.assertIsNotNone(session.query(User).get(self.user.id))

        self.assertIsNone(session.query(Match).get(match_2.id))

    def test_get(self):
        match = MatchService._create(self.create_match())

        self.assertEqual(match.id, MatchService.get(match.id)["id"])

        self.create_players(match)

        with self.assertRaises(MatchServiceAccessException):
            MatchService.get(match.id, self.user.id + 2)

        self.assertEqual(
            MATCH_SCHEMA.dump(match), MatchService.get(match.id, self.user.id)
        )

    def test_get_model_or_rise(self):
        match = MatchService._create(self.create_match())

        self.assertEqual(match, MatchService._get_model_or_rise(match.id))

        with self.assertRaises(InstanceNotFoundException):
            MatchService._get_model_or_rise(match.id + 1)

    def test_create_singleplayer(self):
        match_1_id = MatchService.create_singleplayer(
            self.create_match(startDate=None, players=[]), self.user.id)["id"]

        match_2_id = MatchService.create_singleplayer(
            self.create_match(startDate=None, players=[]), self.user.id)["id"]
        self.assertIsNone(MatchRepository.get(match_1_id))

        MatchService.delete_unbegun(match_2_id)
        self.assertIsNone(MatchRepository.get(match_2_id))

        match_3_id = MatchService.create_singleplayer(
            self.create_match(startDate=None, players=[]), self.user.id)["id"]

        MatchRepository.update(match_3_id, startDate=datetime.utcnow())

        with self.assertRaises(MatchServiceException):
            MatchService.create_singleplayer(
                self.create_match(startDate=None, players=[]),
                self.user.id)

