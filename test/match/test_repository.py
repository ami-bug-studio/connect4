import datetime
import unittest

from app.database import Base, session
from app.match.model import Match
from app.match.repository import MatchRepository
from app.player.repository import PlayerRepository
from app.role.model import Role as RoleModel
from app.user.model import User as UserModel
from manage import app
from test.user import user_dict_generator


class PlayerRepositoryTest(unittest.TestCase):
    user_generator = user_dict_generator()
    match_fields = dict(
        fieldHeight=5, fieldWidth=6, difficulty=1,
        startDate=datetime.datetime.now()
    )

    def setUp(self):
        app.test_request_context().push()
        Base.drop_all()
        Base.create_all()

        self.role = RoleModel(name="player", description="Simple player entity")
        session.add(self.role)

        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        self.user = UserModel(**user_dict)

        user_2_dict = next(self.user_generator)
        user_2_dict["roles"] = [self.role]
        self.user_2 = UserModel(**user_2_dict)
        session.add(self.user_2)
        session.commit()


        # session.add(self.player)

    def tearDown(self):
        session.close()

    def create_players(self, match):
        player_1 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user_id=self.user.id,
            match=match
        )

        player_2 = PlayerRepository.create(
            is_ai=False, is_winner=False, score=0, user_id=self.user_2.id,
            match=match
        )

        return player_1, player_2

    def test_create(self):
        _match = Match(**self.match_fields)
        match = MatchRepository.create(_match)
        self.create_players(match)

    def test_get_by_player(self):
        _match = Match(**self.match_fields)
        match = MatchRepository.create(_match)
        player_1, _ = self.create_players(match)

        self.assertEqual(match, MatchRepository.get_by_player(player_1.id))

    def test_str_dunder(self):
        _match = Match(**self.match_fields)
        match = MatchRepository.create(_match)

        str(match)
