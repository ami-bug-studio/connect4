import unittest

from manage import app, add_initial_player_role
from app.user.model import User as UserModel
from app.role.model import Role as RoleModel
from app.role.repository import RoleRepository
from app.database import Base, session

from test.user import user_dict_generator


class RoleRepositoryTest(unittest.TestCase):

    user_generator = user_dict_generator()

    def setUp(self):
        app.test_request_context().push()
        Base.drop_all()
        Base.create_all()

        session.add(RoleModel(name="player", description="Simple player entity"))
        self.role = session.query(RoleModel).first()
        user_dict = next(self.user_generator)
        user_dict["roles"] = [self.role]
        session.add(UserModel(**user_dict))
        session.commit()

        self.assertEqual(session.query(UserModel).count(), 1)
        self.assertEqual(session.query(RoleModel).count(), 1)

    def tearDown(self):
        session.close()

    def test_get_by_id(self):

        test_role = session.query(RoleModel).first()

        role = RoleRepository.get_by_id(test_role.id)
        self.assertIsNotNone(role)
        self.assertTrue(role.id == test_role.id)

        role = RoleRepository.get_by_id(8500)
        self.assertIsNone(role)

    def test_get_by_name(self):

        test_role = session.query(RoleModel).first()
        session.close()

        role = RoleRepository.get_by_name(test_role.name)
        self.assertIsNotNone(role)
        self.assertTrue(role.id == test_role.id)
        self.assertTrue(role.name == test_role.name)

        role = RoleRepository.get_by_name("admin")
        self.assertIsNone(role)

    def test_add_initial_player_role(self):
        self.assertEqual(session.query(RoleModel).count(), 1)

        session.delete(session.query(RoleModel).first())
        session.commit()
        self.assertEqual(session.query(RoleModel).count(), 0)

        add_initial_player_role()
        self.assertEqual(session.query(RoleModel).count(), 1)
        self.assertEqual(session.query(RoleModel).first().name, "player")

    def test_str_dunder(self):
        role = RoleModel(name="some role", description="random description")
        str(role)
