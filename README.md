# Connect4 Server

This is a server for a Connect4 game created by BUG Studio

## Installation

The following operations are required to start the project

### 1. Install Python __3.7__ 
download from https://www.python.org/downloads/ - seek for the latest release.

### 2. Add the following paths to the environment variables 
More info about flask variables is [here](https://flask.palletsprojects.com/en/1.1.x/config/).

##### Path:

`C:\Users\{User}\AppData\Local\Programs\Python\Python37\Scripts\`

`C:\Users\{User}\AppData\Local\Programs\Python\Python37\`

##### Also add this properties to the environment variables:

*FLASK_APP*:

`manage.py`


*APP_SETTINGS*:

`config.DevelopmentConfig`
or
`config.ProductionConfig`

For development it's proposed to make your own `config.py` copy (use name `config_my.py` which is already in `.gitignore`). 

*DATABASE_URL*:

`postgresql+psycopg2://{login}:{password}@localhost/{db_name}`

The list may not be 100% comprehensive, so read carefully error log.

##### Beware that environment variables can differ per configuration 
Like setting up env in specific PyCharm's run configurations won't have any effect for running commands in a terminal. Use `echo %VAR_MAME%` command on Шindows to test a variable for correctness.

#### 3. Copy the https link and download the project files:
```bash
git clone https://gitlab.com/ami-bug-studio/connect4.git`
```
And don't you forget that project specific commands should be run inside project root dir (where `manage.py` resides) if not said otherwise.

#### 4. Create a virtual environment:
Move to some neat directory (not necessarily project root one, so that venv can be reused later thought not a best practice) and run next command:
```bash
python -m venv env
```

##### Activate environment by command:

```bash
cd env/Scripts && activate
```

#### 5. Set the required dependencies with the command:

for development:
```bash
pip install -r requirements/development.txt
```
or for production:
```bash
pip install -r requirements/production.txt
```
#### 6. Apply database migration script
Run the command:
```bash
flask db upgrade
```
If environment variables set correctly and you have clear database you will get no errors. 


#### 7. Add the Test configuration

from project root run next console command:
```bash
python -m unittest
```
It should print string of dots.

#### 8. Add the Run/Debug Configurations:

Script Path > {project_directory}\manage.py
Parameters > runserver

or simple start project from console by command:

```bash
python manage.py runserver
```

#### 9. Try server web-API using Postman
Download somewhere from [here](https://www.postman.com/) and import (File menu) `connect-4.postman_collection.json` and `connect-4.postman_environment.json` files from project root dir. 

